#!/usr/bin/env python

from Quickload import *
import QuickloadUtils as utils

genome="A_thaliana_Jun_2009"

# contains files on Quickload site
deploy_dir='CCA1-ChIPSeq/'

# folder names for Available Data section of Data Access tab
igb_folder='ChIP-Seq/CCA1/'

# URL user's browser opens when they click the "i" in Available Data section
# URL is relative to the Quickload root
track_info_url=genome+'/'+deploy_dir

# TO-DO - add code for remaining files
# to test, execute
#   makeCca1AnnotsXml.py CCA1.xml
# file name fixes:
#   remove "sort" from BAM files as these are always sorted when
#   deployed via Igbquickload
#   remove "ucsc" from names
#   no need to include "peaks" in BED file names if all BED files
#   contain peaks
#   use consistent naming conventions for BAM, bedgraph files, e.g.,
#   SAMPLE.bam and SAMPLE.bedgraph to make it clear that the bedgraph file
#   and the bam file refer to the same dataset
def makeQuickloadFiles():
    quickload_files = []
    # Input files
    fname=deploy_dir+"Input.bam"
    foreground_color="A5A5A5"
    # description shoulde state succinctly what the file represents
    description="background DNA control read alignments"
    track_name=igb_folder+'Input, Reads'
    quickload_file=BamFile(path=fname,
                           foreground_color=foreground_color,
                           show2tracks=True,
                           tool_tip=description,
                           track_name=track_name,
                           track_info_url=track_info_url)
    quickload_files.append(quickload_file)

    description="background DNA control read alignments coverage graph"
    fname=deploy_dir+"Input.bedgraph.gz"
    track_name=igb_folder+"Input, Graph"
    quickload_file=QuickloadFile(path=fname,
                                 foreground_color=foreground_color,
                                 tool_tip=description,
                                 track_name=track_name,
                                 track_info_url=track_info_url)
    quickload_files.append(quickload_file)

    # should delete "peaks" from name if all BED files in this collection are peaks    
    description="peaks called using HOMER / IDR  for samples from 12:12 light:dark cycle"
    fname=deploy_dir+'/LD.peaks.bed.gz'
    foreground_color="4DA719"
    track_name=igb_folder+"LD Peaks"
    foreground='4DA719'
    quickload_file=QuickloadFile(path=fname,
                                 foreground_color=foreground_color,
                                 tool_tip=description,
                                 track_name=track_name,
                                 track_info_url=track_info_url,
                                 direction_indicator="none",
                                 show2tracks=False)
    quickload_files.append(quickload_file)
    # add the rest of the files
    return quickload_files

if __name__ == '__main__':
    about = "Make annots.xml text for A_thaliana_Jun_2009 CCA1 ChIP-Seq files.\nSee http://www.ncbi.nlm.nih.gov/pubmed/26261339"
    utils.checkForHelp(about)
    quickload_files=makeQuickloadFiles()
    txt = makeFileTagsXml(quickload_files)
    writeToFileStream(txt) 

