from Quickload import *

"""Utility code for making annots.xml files for RNA-Seq experiment files that separate single-mapping (SM) from multi-mapping (MM) reads."""

def makeQuickloadFilesForRNASeq(lsts=None,
                            folder=None,
                            deploy_dir=None,
                            all=False):
    """
    Function: Create list of QuickloadFile objects representing data files from an
              RNA-Seq experiment, organized into subfolders of multi-mapping (MM)
              and single-mapping (SM) reads. Assumes all data files from the same
              sample have the same base file name and there are reads, coverage,
              and junctions file for each sample. 
    Args    : folder - shown to user in IGB, folder containing the samples
              deploy_dir - physical folder containing the files on the Quickload site
              all - if True, also include files with both SM and MM reads
              lsts - list of 4 or 5-item lists containing strings:
           1) data file base name, e.g, C1 for C1.bam, C1.bedgraph, etc
           2) sample name to be show to users in IGB, e.g., Control Rep 1 
           3) features color for annotations or graphs, in hexadecimal (e.g., FFFFFF for white)
           4) background color for the track to be shown in IGB
           5) (optional) URL opened when user clicks "i" (info) icon
              next to data set
              note: URL is absolute (e.g., http://www.example.com) or
                    relative to the quickload root

              example)
                [['BA2h-R1','BA Root1','a tooltip','O_sativa_japonica_Oct_2011/JK-TH2.0.5_processed']]
    Returns : List of QuickloadFile objects.
    """
    quickload_files = [] 
    for lst in lsts:
        f = lst[0] 
        sample=lst[1]
        foreground=lst[2]
        background=lst[3]
        url = None
        if len(lst)>4:
            url = lst[4]
        # SM alignments
        descr = "alignments for reads that mapped once onto the genome"
        fn = deploy_dir + "/" + f + '.sm.bam'
        title = folder + "/SM/Reads/" + sample + ", SM alignments"
        quickload_file = BamFile(path=fn,track_name=title,track_info_url=url,
                                 foreground_color=foreground,background_color=background,
                                 tool_tip=descr)
        quickload_files.append(quickload_file)
        # MM alignments
        descr = "alignments for reads that mapped to multiple places in the genome"
        fn = deploy_dir + "/" + f + '.mm.bam'
        title = folder + "/MM/Reads/" + sample + ", MM alignments"
        quickload_file = BamFile(path=fn,track_name=title,track_info_url=url,
                                 foreground_color=foreground,background_color=background,
                                 tool_tip=descr)
        quickload_files.append(quickload_file)
        if all: # if also have file with both SM and MM 
            descr = "alignments for ALL reads (multi- and single-mapping)"
            fn = deploy_dir + "/" + f + '.bam'
            title = folder + "/All/Reads/" + sample + ", All alignments"
            quickload_file = BamFile(path=fn,track_name=title,track_info_url=url,
                                    foreground_color=foreground,background_color=background,
                                    tool_tip=descr)
            quickload_files.append(quickload_file)
        # SM coverage graph
        descr = "coverage graph for reads that mapped once onto the genome"
        fn = deploy_dir + "/" + f + '.sm.bedgraph.gz'
        title = folder + "/SM/Graph/" + sample + ", SM coverage"
        quickload_file = QuickloadFile(path=fn,track_name=title,track_info_url=url,
                                       foreground_color=foreground,background_color=background,
                                       tool_tip=descr)
        quickload_files.append(quickload_file)
        # MM coverage graph
        descr = "coverage graph for reads that mapped to multiple places in the genome"  
        fn = deploy_dir + "/" + f + '.mm.bedgraph.gz'
        title = folder + "/MM/Graph/" + sample + ", MM coverage"
        quickload_file = QuickloadFile(path=fn,track_name=title,track_info_url=url,
                                       foreground_color=foreground,background_color=background,
                                       tool_tip=descr)
        quickload_files.append(quickload_file)
        # all coverage graph
        if all:
            descr = "coverage graph for all reads (multi- and single-mapping)"
            fn = deploy_dir + "/" + f + '.bedgraph.gz'
            title = folder + "/All/Graph /" + sample + ", all coverage"
            quickload_file = QuickloadFile(path=fn,track_name=title,track_info_url=url,
                                        foreground_color=foreground,background_color=background,
                                        tool_tip=descr)
            quickload_files.append(quickload_file)

        # tophat junction file, from all the reads
        descr = "Junctions predicted by TopHat"
        fn = deploy_dir + "/" + f + '.bed.gz'
        title = folder + "/TH Juncs/" + sample + ", tophat junctions"
        quickload_file = JunctionFile(path=fn,track_name=title,track_info_url=url,
                                       foreground_color=foreground,background_color=background,
                                       tool_tip=descr)
        quickload_files.append(quickload_file)
        # FindJunctions junction file, from SM reads only
        descr = "Junctions predicted by Loraine Lab FindJunctions, single-mapping reads only"
        fn = deploy_dir + "/" + f + '.sm.FJ.bed.gz'
        title = folder + "/FJ Juncs/" + sample + ", LL SM junctions"
        quickload_file = JunctionFile(path=fn,track_name=title,track_info_url=url,
                                       foreground_color=foreground,background_color=background,
                                       tool_tip=descr)
        quickload_files.append(quickload_file)
    return quickload_files
