#!/usr/bin/env python

"""Make annots.xml for Arabidopsis TAIR10/9 genome assembly. Requires CCA1.xml, LFY-ChIP-Chip.xml, LFY-ChIP-Seq.xml, ARR10-SRP048935.txt. These are version-controlled in A_thaliana_Jun_2009 genome directory in the Quickload svn repository. Run this in that directory to create the annots.xml file for this genome version release."""

from make_genome_annots import *
from Quickload import *
from AnnotsXmlForSmMmRNASeq import *
import QuickloadUtils as utils

import os,sys

genome="A_thaliana_Jun_2009"

# rgb # 0, 124, 0
TDNAFOREGROUND = '007c00' 


tair10_annotfiles = """TAIR10_mRNA.bed.gz\tTAIR10 mRNA\tprotein-coding gene models\tA_thaliana_Jun_2009
TAIR10.bed.gz\tTAIR10 other annotations/ALL GENES\tAll gene annotations. Anything with a name like AT1G12345, including transposable element genes.\tA_thaliana_Jun_2009
TAIR10_TE.bed.gz\tTAIR10 other annotations/transposable elements\ttransposable elements
TAIR10_TE_gene.bed.gz\tTAIR10 other annotations/TE genes\ttransposable element gene models
TAIR10_miRNA.bed.gz\tTAIR10 other annotations/miRNA genes\tmiRNA gene models
TAIR10_ncRNA.bed.gz\tTAIR10 other annotations/ncRNA genes\tsmall non-coding RNA gene models
TAIR10_pseudogene.bed.gz\tTAIR10 other annotations/pseudogenes\tpseudogenic transcript models
TAIR10_rRNA.bed.gz\tTAIR10 other annotations/rRNA genes\trRNA gene models
TAIR10_snoRNA.bed.gz\tTAIR10 other annotations/snoRNA genes\tsmall nucleolar RNA gene models
TAIR10_tRNA.bed.gz\tTAIR10 other annotations/tRNA genes\ttRNA gene models
TAIR10_snRNA.bed.gz\tTAIR10 other annotations/snRNA genes\tsmall nuclear RNA gene models"""

# Thes files are version-controlled in https://svn.transvar.org/repos/genomes/trunk/pub/quickload/A_thaliana_Jun_2009
include_fnames=['CCA1.xml','LFY-ChIP-Chip.xml','LFY-ChIP-Seq.xml']

def makeAraportAnnotationFile():
    return AnnotationFile(path="Araport11.bed.gz",
                            track_name="Araport",
                            show2tracks=True,
                            track_label_font_size=14,
                            tool_tip="Protein-coding gene models from Araport 11, Nov. 2015",
                            foreground_color="000000",
                            track_info_url=genome)

def makeAtRTDv2AnnotationFile():
    return AnnotationFile(path="AtRTD2_19April2016_wDescr.bed.gz",
                            track_name="AtRTDv2",
                            show2tracks=True,
                            track_label_font_size=14,
                            tool_tip="Gene models from Arabidopsis reference transcriptome, published April 2016",
                            foreground_color="000000",
                            track_info_url=genome)


def makeColdStressRNASeqFiles():
    mixed_cold_study_name = "RNA-Seq/Loraine Lab/Mixed Cold"
    cold_study_deploy_dir='cold_stress'
    url='A_thaliana_Jun_2009/cold_stress/'
    bg='FFFFFF'
    lsts = [["cold_control","Control",'006600',bg,url],["cold_treatment","Cold","4D4DFF",bg,url]]
    return makeQuickloadFilesForRNASeq(lsts=lsts,folder=mixed_cold_study_name,
                                       deploy_dir=cold_study_deploy_dir,all=True)


def makeQuickloadFiles():
    # Araport
    quickload_files = [makeAraportAnnotationFile()]
    quickload_files.append(makeAtRTDv2AnnotationFile())
    # T-DNA insertion lines
    quickload_files.append(q.AnnotationFile(path="TDNA.bed.gz",track_name="TDNA insertion lines",
                            tool_tip="insertion lines, start of block marks insertion site",
                            track_info_url="A_thaliana_Jun_2009",show2tracks=False,
                            foreground_color=TDNAFOREGROUND,direction_indicator="none"))
    # TAIR10 annotations
    for quickload_file in makeQuickloadFilesForGenomeRelease(files_string=tair10_annotfiles,
                                                             reference_models_index=0,genome=genome):
        quickload_files.append(quickload_file)
    # RNA-Seq
    for quickload_file in makeColdStressRNASeqFiles():
        quickload_files.append(quickload_file)
    # ARR10 ChIP-Seq
    filename="ARR10_SRP048935.txt"
    for quickload_file in utils.makeQuickloadFileObjectsFromTableData(fname=filename):
        quickload_files.append(quickload_file)
    return quickload_files

if __name__ == '__main__':
    about="Make annots.xml text for A_thaliana_Jun_2009 assembly." 
    utils.checkForHelp(about)
    quickload_files=makeQuickloadFiles()
    main(quickload_files=quickload_files,
         include_fnames=include_fnames)
