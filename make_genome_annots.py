#!/usr/bin/env python

"""Utlity methods for making annots.xml for a genome release"""

import Quickload as q
import os,sys

ref_models_foreground='000000'
track_label_font_size=14

def makeQuickloadFilesForGenomeRelease(files_string=None,
                                       reference_models_index=None,
                                       genome=None):
    quickload_files = []
    lsts=map(lambda x:x.split('\t'),files_string.split('\n'))
    i=0
    for lst in lsts:
        if len(lsts)==4:
            track_info_url=lsts[3]
        else:
            track_info_url=genome
        quickload_file = q.AnnotationFile(path=lst[0],track_name=lst[1],
                                       track_label_font_size=track_label_font_size,
                                       track_info_url=track_info_url,show2tracks=True,
                                       tool_tip=lst[2])
        if i == reference_models_index:
            quickload_file.setLoadAll(True)
            quickload_file.setForegroundColor(ref_models_foreground)
        quickload_files.append(quickload_file)
        i = i + 1
    return quickload_files

