
from Quickload import *

def makeQuickloadFilesForRNASeq(lsts=None,
                            folder=None,
                            deploy_dir=None,
                            include_FJ=True):
    quickload_files = []
    for lst in lsts:
        f = lst[0]
        sample=lst[1]
        foreground=lst[2]
        background=lst[3]
        url = None
        if len(lst)>4:
            url = lst[4]
        # alignments
        descr = "read alignments"
        fn = deploy_dir + "/" + f + '.bam'
        title = folder + "/Reads/" + sample + " alignments"
        quickload_file = BamFile(path=fn,track_name=title,track_info_url=url,
                                 foreground_color=foreground,background_color=background,
                                 tool_tip=descr)
        quickload_files.append(quickload_file)

        # coverage
        descr = "coverage graph"
        fn = deploy_dir + "/" + f + '.bedgraph.gz'
        title = folder + "/Graph/" + sample + " coverage"
        quickload_file = QuickloadFile(path=fn,track_name=title,track_info_url=url,
                                       foreground_color=foreground,background_color=background,
                                       tool_tip=descr)

        
        quickload_files.append(quickload_file)

        # tophat junctions
        descr = "Junctions predicted by TopHat"

        if include_FJ:
            descr = "Junctions predicted by Loraine Lab FindJunctions, single-mapping reads only"
            fn = deploy_dir + "/" + f + '.FJ.bed.gz'
            title = folder + "/Junctions/" + sample + " LL junctions"
            quickload_file = JunctionFile(path=fn,track_name=title,track_info_url=url,
                                       foreground_color=foreground,background_color=background,
                                       tool_tip=descr)
            quickload_files.append(quickload_file)
        fn = deploy_dir + "/" + f + '.bed.gz'
        title = folder + "/Junctions/" + sample + " tophat junctions"
        quickload_file = JunctionFile(path=fn,track_name=title,track_info_url=url,
                                       foreground_color=foreground,background_color=background,
                                       tool_tip=descr)
        quickload_files.append(quickload_file)
    return quickload_files

