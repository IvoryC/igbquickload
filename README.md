# Annots.xml utilities for IGB Quickload #

This repository contains python code for writing annots.xml files 
for IGB Quickload sites.

See [About annots.xml](https://wiki.transvar.org/display/igbman/About+annots.xml)

## Setting up

* Clone the repo
* Add the cloned repo to your PATH